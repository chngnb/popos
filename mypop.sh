#!/bin/bash

PROGRAM="$0"
CUR_DIR="${PROGRAM%/*}"
PREV_DIR="${CUR_DIR%/*}"

update()
{
	sudo apt update
	sudo apt upgrade
}

install_brave()
{
	sudo apt install apt-transport-https curl

	sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg

	echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list

	sudo apt update

	sudo apt install brave-browser
}

install_misc()
{
	sudo apt install duf fzf bat zoxide arandr ffmpeg imagemagick gnome-tweaks openssh-server htop bpytop
}

install_pypkgs()
{
	sudo apt install python3-pip
	pip install yt-dlp
}

install_nvim()
{
	wget https://github.com/neovim/neovim/releases/download/nightly/nvim.appimage
	mkdir -p ~/.local/bin
	mv nvim.appimage ~/.local/bin/nvim
	chmod +x ~/.local/bin/nvim
}

install_lvim()
{

	sudo apt install -y npm
	bash <(curl -s https://raw.githubusercontent.com/lunarvim/lunarvim/master/utils/installer/install.sh)
}

install_rustpkgs()
{
	sudo apt install cargo
	cargo install lsd

	#sudo dpkg -i lsd_0.21.0_amd64.deb
}

install_zsh()
{
	sudo apt install zsh zsh-autosuggestions zsh-syntax-highlighting
	chsh -s /bin/zsh $USER
	[ -d ~/Dotfiles ] || git clone https://gitlab.com/chngnb/popos ~/Dotfiles/
	
}

set_configs()
{
  ln -sr $CUR_DIR/lvim ~/.config/lvim
  ln -sr $CUR_DIR/zsh/.zshrc ~/.zshrc
}

install_fonts()
{

  for f in $CUR_DIR/fonts/*.zip
  do
    dir=${f##*/}
    dir=${dir%.zip}
    mkdir -p ~/.local/share/fonts/"$dir"
    unzip "$f" -d ~/.local/share/fonts/"$dir"
  done
  fc-cache -f -v
}

install_themes()
{
  for f in $CUR_DIR/themes/*.zip
  do
    dir=${f##*/}
    dir=${dir%.zip}
    mkdir -p ~/.local/share/themes/"$dir"
    unzip "$f" -d ~/.local/share/themes/"$dir"
  done

  for f in $CUR_DIR/themes/*.tar.xz
  do
    dir=${f##*/}
    dir=${dir%.tar.xz}
    mkdir -p ~/.local/share/themes/"$dir"
    tar xvf "$f" --directory ~/.local/share/themes/"$dir"
  done
}


update
install_brave
install_misc
install_pypkgs
install_nvim
install_lvim
install_rustpkgs
install_zsh
set_configs
install_fonts
install_themes
update
