[[ $- != *i* ]] && return

typeset -g comppath="${XDG_CACHE_HOME:-$HOME/.cache}/zsh"
typeset -g compfile="$comppath/zcompdump"

if [[ -d "$comppath" ]]; then
    [[ -w "$compfile" ]] || rm -rf "$compfile" >/dev/null 2>&1
else
    mkdir -p "$comppath"
fi

SHELL=$(command -v zsh || echo '/bin/zsh')
KEYTIMEOUT=1
SAVEHIST=10000
HISTSIZE=10000
HISTFILE="$comppath/zsh_history"
setopt EXTENDED_HISTORY
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_FIND_NO_DUPS
setopt HIST_SAVE_NO_DUPS
setopt HIST_BEEP

zshsyntax=/usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
zshsuggts=/usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh
[[ -f "$zshsyntax" ]] && source "$zshsyntax"
[[ -f "$zshsuggts" ]] && source "$zshsuggts"
unset zshsyntax zshsuggts

alias ls="lsd --group-dirs first -lAh"
alias ll="lsd --group-dirs first -lh"
alias grep="grep --color=auto"
#alias vrc="nvim ~/.config/nvim/init.lua"
alias off="shutdown now"
alias yta="yt-dlp --no-playlist -f bestaudio"
alias ytb="yt-dlp --no-playlist -f bestvideo+bestaudio"
alias ytd="yt-dlp"
alias zshrc="lv ~/.zshrc"
alias q="exit"
alias install="sudo apt install"
alias search="apt search"
alias purge="sudo apt purge"
alias update="sudo apt update && sudo apt upgrade"
alias remove="sudo apt remove"
alias autoremove="sudo apt autoremove"
alias nv="nvim"
alias lv="lvim"
alias d="z"
alias bat="batcat"

export LC_COLLATE=C
export PROMPT='%F{green}%n@%m%f:%~♠ '
export PROMPT2="-> "

# manpager colours
export MANWIDTH=120
export MANPAGER="sh -c 'col -bx | batcat -l man -p'" # defaults to less
typeset -U PATH path
path=(
    "$HOME/.local/bin"
    "$HOME/.cargo/bin"
    "$path[@]"
)
export PATH
eval "$(zoxide init zsh)"
ex ()
{
    if [[ -f $1 ]] ; then
      case $1 in
        *.tar.bz2)   tar xjf $1   ;;
        *.tar.gz)    tar xzf $1   ;;
        *.bz2)       bunzip2 $1   ;;
        *.rar)       unrar x $1   ;;
        *.gz)        gunzip $1    ;;
        *.tar)       tar xf $1    ;;
        *.tbz2)      tar xjf $1   ;;
        *.tgz)       tar xzf $1   ;;
        *.zip)       unzip $1     ;;
        *.Z)         uncompress $1;;
        *.7z)        7z x $1      ;;
        *.deb)       ar x $1      ;;
        *.tar.xz)    tar xf $1    ;;
        *.tar.zst)   unzstd $1    ;;
        *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

